CREATE DATABASE test;

use test;

CREATE TABLE tbPatients 
(
	id varchar(36), 
	json text, 
	birthDate datetime
);