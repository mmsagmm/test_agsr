﻿namespace PatientService.RequestModels
{
    public class UpdatePatientRequestModel : BasePatientRequestModel
    {
        public Guid Id { get; set; }
    }
}
