﻿
using PatientService.RequestModels;

namespace PatientService.Validation
{
    public class CreatePatientModelValidation : BasePatientModelValidation<CreatePatientRequestModel>

    {
        public CreatePatientModelValidation()
        {
        }
    }
}
