﻿namespace Db.DataAccess.Options
{
    public class MySqlOption
    {
        public string? ConnectionString { get; set; }
    }
}
