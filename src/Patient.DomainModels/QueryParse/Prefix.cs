﻿namespace Patient.DomainModels.QueryParse
{

    public enum Prefix
    {
        Empty,
        Equal,
        LessThan,
        GraterThan,
        GreaterOrEqual,
        LessOrEqual,
        StartsAfter,
        EndBefore,
        Approximately
    }

}
